
<!-- start footer -->
<div class="footer">
	
	Copyright <?php echo date("Y"); ?> &copy; <?php bloginfo('name'); ?>, All rights reserved.
	
</div>
<!-- end footer -->

<script src="<?php bloginfo('stylesheet_directory'); ?>/dist/js/scripts.js"></script>

<?php wp_footer(); ?>

</body>
</html>