<h2>What Our Clients Say...</h2>


<?php 
$coursetype = get_field('link_course_type');
if(!$coursetype){
	$slug = get_post_field( 'post_name', get_post() );
}else{
	$coursetype = get_field('link_course_type');
	$slug = $coursetype->slug;
}
$taxquery = array(
	array(
		'taxonomy' => 'course_type',
		'field'    => 'slug',
		'terms'    => $slug,
	),
);
$args = array( 
	'post_type' => 'testimonial',
	'posts_per_page' => 1,
	'orderby' => 'rand',
	'tax_query' => $taxquery
);
$the_query = new WP_Query( $args );
// The Loop
if ( $the_query->have_posts() ) :
while ( $the_query->have_posts() ) : $the_query->the_post();
	$display_title = (get_field('display_title')) ? get_field('display_title') : '';
?>
<div class="text"><?php the_content(); ?>
</div>
<div class="quote_by">
	<?php echo $display_title; ?>
</div>

<?php
endwhile;
endif;
// Reset Post Data
wp_reset_postdata();
?>