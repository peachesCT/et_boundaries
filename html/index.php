<!DOCTYPE html>
<html lang="en">
	<head>
		
		<meta charset="utf-8">
		<title>Site Name</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="SKYPE_TOOLBAR" CONTENT="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
		<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if lt IE 7]><script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script><![endif]-->
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="css/main.css">
		<!-- (for production, also delete previous line) <link rel="stylesheet" href="dist/css/main.css"> -->
	</head>
	<body class="footer_orange">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/home_header.php"; ?>
		<!-- end header -->

		<div class="section intro">
			<div class="row">
				<div class="has_2_cols intro">
					<div class="col">
						<div class="icon"><img src="images/people.png" alt=""></div>
						<h2 class="title">For People</h2>
						<div class="text">Training | Maintenance Group <br>Training Packages | Bespoke Training</div>
					</div>
					<div class="col">
						<div class="icon"><img src="images/organisations.png" alt=""></div>
						<h2 class="title">For Organisations</h2>
						<div class="text">Prevention | Bespoke Training <br>Remediation</div>
					</div>
				</div>
			</div>
		</div>

		<div class="section courses">
			<div class="row">
				<h2>Our Courses</h2>
				<div class="has_2_cols">
					<div class="col">
						<h4 class="title title_red">Maintaining Professional Boundaries</h4>
						<div class="text">Lorium ipsum blah blah bling Lorium ipsum blah blah bling Lorium
ipsum blah blah bling Lorium ipsum blah blah bling</div>
						<div class="cta"><a href="#">Is This Course Right For Me?</a></div>
					</div>

					<div class="col">
						<h4 class="title title_blue">Maintaining Professional Relationships</h4>
						<div class="text">Lorium ipsum blah blah bling Lorium ipsum blah blah bling Lorium
ipsum blah blah bling Lorium ipsum blah blah bling</div>
						<div class="cta"><a href="#">Is This Course Right For Me?</a></div>
					</div>

					<div class="col">
						<h4 class="title title_green">Maintaining Professional Ethics</h4>
						<div class="text">Lorium ipsum blah blah bling Lorium ipsum blah blah bling Lorium
ipsum blah blah bling Lorium ipsum blah blah bling</div>
						<div class="cta"><a href="#">Is This Course Right For Me?</a></div>
					</div>

					<div class="col">
						<h4 class="title title_gold">Boundaries In Practice</h4>
						<div class="text">Lorium ipsum blah blah bling Lorium ipsum blah blah bling Lorium
ipsum blah blah bling Lorium ipsum blah blah bling</div>
						<div class="cta"><a href="#">Is This Course Right For Me?</a></div>
					</div>

					<div class="col">
						<h4 class="title title_avo">Values In Practice</h4>
						<div class="text">Lorium ipsum blah blah bling Lorium ipsum blah blah bling Lorium
ipsum blah blah bling Lorium ipsum blah blah bling</div>
						<div class="cta"><a href="#">Is This Course Right For Me?</a></div>
					</div>

					<div class="col">
						<h4 class="title title_char">Listening In Practice</h4>
						<div class="text">Lorium ipsum blah blah bling Lorium ipsum blah blah bling Lorium
ipsum blah blah bling Lorium ipsum blah blah bling</div>
						<div class="cta"><a href="#">Is This Course Right For Me?</a></div>
					</div>
				</div>
			</div>
		</div>

		<div class="section pull_quote">
			<div class="row">
				<div class="pull_quote full">
					<?php include "templates/quote.php"; ?>
				</div>
			</div>
		</div>
		
		<script src="dist/js/scripts.js"></script>
	</body>
</html>