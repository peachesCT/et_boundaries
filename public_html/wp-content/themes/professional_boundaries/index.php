<?php get_header(); ?>

	<body class="footer_orange">
		<a href="<?php bloginfo('url'); ?>/covid-19/" style="position:absolute;top:10px;
		left:50%;margin-left:-165px;background:red;color:white;border-radius:5px;padding:5px 10px;text-decoration: none;font-size:13px;z-index:9;">IMPORTANT INFORMATION REGARDING COVID-19</a>
		<!-- start header -->
		<div class="vid_container">
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/home_header.php"; ?>
		<!-- <video id="bgvid" playsinline autoplay muted loop>
		<source src="<?php //bloginfo('stylesheet_directory'); ?>/images/pb.mp4" type="video/mp4">
		</video> -->
		</div>
		<!-- end header -->

		<div class="section intro">
			<div class="row">
				<div class="has_2_cols intro">
					<div class="col">
						<a href="<?php bloginfo('url'); ?>/for-people/">
							<div class="icon"><img src="<?php bloginfo('template_url'); ?>/images/people.png" alt=""></div>
						<h2 class="title">For People</h2>
						<div class="text">Training | Training Packages<br> Bespoke Training</div>
						</a>
					</div>
					<div class="col">
						<a href="<?php bloginfo('url'); ?>/for-organisations/">
							<div class="icon"><img src="<?php bloginfo('template_url'); ?>/images/organisations.png" alt=""></div>
						<h2 class="title">For Organisations</h2>
						<div class="text">Prevention | Bespoke Training <br>Remediation</div>
						</a>
					</div>
				</div>
			</div>
		</div>

		<div class="section courses">
			<div class="row">
				<h2>Our Courses</h2>
				<div class="has_2_cols">

				<?php 
					    $terms = get_terms( 'course_type', array(
						    'hide_empty' => false,
						) );

						//print_r($terms);

						foreach($terms as $term){
							$header_color = (get_field('header_color', 'course_type_'.$term->term_id)) ? get_field('header_color', 'course_type_'.$term->term_id) : '';
				?>
					<div class="col">
						<h4 class="title <?php echo $header_color; ?>"><?php echo $term->name; ?></h4>
						<div class="text">
						<?php echo $term->description; ?>
						</div>
						<div class="cta"><a href="<?php echo get_permalink(get_page_by_path($term->slug)); ?>">Find out more...</a></div>
					</div>
				<?php
						}
				?>




				</div>
			</div>
		</div>

		<div class="section pull_quote">
			<div class="row">
				<div class="pull_quote full">
					<?php include "templates/quote-slider.php"; ?>
				</div>
			</div>
		</div>
		
<?php get_footer(); ?>