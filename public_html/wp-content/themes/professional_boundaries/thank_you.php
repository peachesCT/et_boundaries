<?php 
/*
	Template Name: Thank you page
*/
get_header(); ?>


<?php if (have_posts()) { while (have_posts()) { the_post();  ?>

	<body class="page_gold footer_gold">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/about_header.php"; ?>
		<!-- end header -->

		<div class="section content">
			<div class="row">
				<?php the_content(); ?>
			</div>
		</div>
		
<?php } } ?>

<?php get_footer(); ?>