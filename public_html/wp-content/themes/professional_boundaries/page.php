<?php 
/*
	Template Name: Thank you page
*/
get_header(); ?>


<?php if (have_posts()) { while (have_posts()) { the_post();  
	
$page_color = (get_field('page_color')) ? get_field('page_color') : 'page_pink';
	$footer_color = (get_field('footer_color')) ? get_field('footer_color') : 'footer_gold';
	?>

	<body class="<?php echo $page_color; ?> <?php echo $footer_color; ?>">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/about_header.php"; ?>
		<!-- end header -->

		<div class="section content">
			<div class="row">
				<?php the_content(); ?>
			</div>
		</div>
		
<?php } } ?>

		<div class="section pull_quote">
			<div class="row">
				<div class="pull_quote full">
					<?php include "templates/quote-slider.php"; ?>
				</div>
			</div>
		</div>

<?php get_footer(); ?>