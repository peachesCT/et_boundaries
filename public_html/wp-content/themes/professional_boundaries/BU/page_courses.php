<?php 
/*Template name: Our Courses page*/
get_header(); ?>

	<body class="page_blue footer_blue">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/courses_header.php"; ?>
		<!-- end header -->

		<div class="section content">
			<div class="row">
				<?php 
				if (have_posts()) { while (have_posts()) { the_post();  

					the_content();

				} } 
				?>
			</div>
		</div>
				
		<div class="section courses">
			<div class="row row_wrap">
				<div class="has_2_cols">
					
				<?php 
					    $terms = get_terms( 'course_type', array(
						    'hide_empty' => false,
						) );

						foreach($terms as $term){
							$header_color = (get_field('header_color', 'course_type_'.$term->term_id)) ? get_field('header_color', 'course_type_'.$term->term_id) : '';
							$course_type_category = (get_field('course_type_category', 'course_type_'.$term->term_id)) ? get_field('course_type_category', 'course_type_'.$term->term_id) : '';
				?>
					<div class="col <?php echo (is_array($course_type_category)) ? implode(" ",$course_type_category) : $course_type_category; ?> <?php echo ( $course_type_category == 'one-day-course' || (is_array($course_type_category) && in_array('one-day-course',$course_type_category)) ) ? 'active' : ''; ?>">
						<h4 class="title <?php echo $header_color; ?>"><?php echo $term->name; ?></h4>
						<div class="text">
						<?php echo $term->description; ?>
						</div>
						<div class="cta"><a href="<?php echo get_permalink(get_page_by_path($term->slug)); ?>">Find out more...</a></div>
					</div>
				<?php
						}
				?>
				
				</div>
				<div class="button-wrapper" style="display:none;">
					<a class="big_button pink_bg" href="#">Discover Which Course Is Right For You</a>
				</div>
			</div>
		</div>

		<div class="section pull_quote">
			<div class="row">
				<div class="pull_quote full">
					<?php include "templates/quote-slider.php"; ?>
				</div>
			</div>
		</div>

		<?php if(get_field('see_more_link')){ ?>
		<div class="link_box" style="padding-top:0;"><a href="<?php echo get_field('see_more_link') ?>">See More</a></div>
		<style>
		.pull_quote.full{
			padding-bottom:0!important;
		}
		</style>
		<?php } ?>
		
<?php get_footer(); ?>
<style>
	.section.courses{
		padding-top:0 !important;
	}
	.section.content{
		padding-bottom:0 !important;
	}
	.section.courses .col{
		display:none !important;
	}
	.section.courses .col.active{
		display:block !important;
	}
	.su-tabs-nav span:nth-of-type(1){
		background: #5b799f !important;
	}
	.su-tabs-nav span:nth-of-type(1).su-tabs-current{
		background:white!important;
		border-color: #5b799f !important;
	}
	.su-tabs-nav span:nth-of-type(2){
		background: #7ca2d3 !important;
	}
	.su-tabs-nav span:nth-of-type(2).su-tabs-current{
		background:white!important;
		border-color: #7ca2d3 !important;
	}
	.su-tabs-nav span:nth-of-type(3){
		background: #97bef4 !important;
	}
	.su-tabs-nav span:nth-of-type(3).su-tabs-current{
		background:white!important;
		border-color: #97bef4 !important;
	}
	.su-tabs-nav span:nth-of-type(4){
		background: #bbd8fa !important;
	}
	.su-tabs-nav span:nth-of-type(4).su-tabs-current{
		background:white!important;
		border-color: #bbd8fa !important;
	}

	.section.courses .has_2_cols .col {
	    padding: 0 20px 20px 0;
	}
</style>
<script>
	/*remove*/
	jQuery('body').on('click','.su-tabs-nav span:nth-of-type(1)',() =>{
		jQuery('.section.courses .col').each(function(){
			jQuery(this).removeClass('active');
		});
	});
	jQuery('body').on('click','.su-tabs-nav span:nth-of-type(2)',() =>{
		jQuery('.section.courses .col').each(function(){
			jQuery(this).removeClass('active');
		});
	});
	jQuery('body').on('click','.su-tabs-nav span:nth-of-type(3)',() =>{
		jQuery('.section.courses .col').each(function(){
			jQuery(this).removeClass('active');
		});
	});
	jQuery('body').on('click','.su-tabs-nav span:nth-of-type(4)',() =>{
		jQuery('.section.courses .col').each(function(){
			jQuery(this).removeClass('active');
		});
	});

	/*add*/
	jQuery('body').on('click','.su-tabs-nav span:nth-of-type(1)',() =>{
		jQuery('.section.courses .col.one-day-course').each(function(){
			jQuery(this).addClass('active');
		});
	});
	jQuery('body').on('click','.su-tabs-nav span:nth-of-type(2)',() =>{
		jQuery('.section.courses .col.three-day-course').each(function(){
			jQuery(this).addClass('active');
		});
	});
	jQuery('body').on('click','.su-tabs-nav span:nth-of-type(3)',() =>{
		jQuery('.section.courses .col.bespoke-courses').each(function(){
			jQuery(this).addClass('active');
		});
	});
	jQuery('body').on('click','.su-tabs-nav span:nth-of-type(4)',() =>{
		jQuery('.section.courses .col.follow-on-courses').each(function(){
			jQuery(this).addClass('active');
		});
	});
</script>