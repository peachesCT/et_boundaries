<div class="section site_nav">
	<div class="row navigator">
		<div class="site-logo">
			<a href="">Professional Boundaries CIC</a>
		</div>
		<div class="nav_section">
			<div class="menu_ico">
				<a href="javascript:;" class="btn_menu">
					menu
					<span class="nav_ico">
						<div class="bar1"></div>
						<div class="bar2"></div>
						<div class="bar3"></div>
					</span>
				</a>
				<div class="btn_close">
					menu
					<span class="nav_ico">
						<div class="bar1"></div>
						<div class="bar3"></div>
					</span>
				</div>
			</div>
			<ul id="menu-top-menu" class="menu">
				<li class="current-menu-item current_page_item"><a href="index.php">Home</a></li>
				<li class=""><a href="page_landing.php">About</a></li>
				<li class=""><a href="page_courses.php">Our Courses</a></li>
				<li class=""><a href="page_booking1.php">For People</a></li>
				<li class=""><a href="page_booking2.php">For Organisations</a></li>
				<li class=""><a href="">Blog</a></li>
				<li class=""><a href="">Contact Us</a></li>
			</ul>
		</div>
	</div>
</div>