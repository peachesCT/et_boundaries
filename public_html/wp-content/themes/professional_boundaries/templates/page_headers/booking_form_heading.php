<?php 
$link_course_type = get_field('link_course_type');
?>

<div class="section site_header">
	<div class="row">
		<h1><?php echo $link_course_type->name; ?></h1>
	</div>
</div>