<?php 
/*Template name: Booking form page*/
get_header(); ?>

<?php if (have_posts()) { while (have_posts()) { the_post(); 
	$form_code = (get_field('form_code')) ? get_field('form_code') : '' ;
	$page_color = (get_field('page_color')) ? get_field('page_color') : 'page_pink';
	$footer_color = (get_field('footer_color')) ? get_field('footer_color') : 'footer_gold';
?>

	<body class="<?php echo $page_color; ?> <?php echo $footer_color; ?>">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/booking_form_heading.php"; ?>
		<!-- end header -->
		<div class="section content">
			<div class="row row_wrap">
				<div class="has_2_cols content">
					<div class="col col_left">
  						<h2>About The Course</h2>
						<?php the_content(); ?>

						<h2>Upcoming Course Dates</h2>

						<div class="course_date_container clearfix">

						<?php 
						$nocourses = true;
						$pagelist = get_pages("child_of=".$post->ID."&parent=".$post->ID."&sort_column=menu_order&sort_order=asc");
						$formpagelink = get_permalink($pagelist[0]->ID);
						$link_course_type = get_field('link_course_type');
						$pagelink = get_the_permalink();
						$taxquery = array(
							array(
								'taxonomy' => 'course_type',
								'field'    => 'slug',
								'terms'    => $link_course_type->slug,
							),
						);

						$args = array(
							'post_type' => 'course',
							'posts_per_page' => -1,
							'tax_query' => $taxquery,
							'meta_key' => 'start_date', 
							'orderby' => 'meta_value', 
							'order' => 'ASC'
						); 

						$the_query = new WP_Query( $args );
						// The Loop
						if ( $the_query->have_posts() ) {
						while ( $the_query->have_posts() ) : $the_query->the_post();

						$start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
						$course_location = (get_field('course_location')) ? get_field('course_location') : '' ;
						$address = (get_field('address')) ? get_field('address') : '' ;
						$nocourses = false;
						$from_and_to_time = (get_field('from_and_to_time')) ? get_field('from_and_to_time') : '' ;
					?>


					<div class="course_item"><span class="course_dates date"><?php echo $link_course_type->name; ?> - <?php echo $start_date; ?></span>
					<span class="course_dates venue"><?php echo $from_and_to_time; ?></span>
					<span class="course_location venue"><?php echo $course_location; ?></span></div>

					<?php 
						endwhile;
						}else{ echo '<p>No upcoming courses.</p>'; }
						// Reset Post Data
						wp_reset_postdata();
					?>

					</div>
						

						<div class="pull_quote left">
							<?php include "templates/quote.php"; ?>
						</div>
					</div>
					<div class="col col_right has_form">
						<h2>Book Your Place</h2>
						<?php if($form_code != '' && !$nocourses ){ echo do_shortcode($form_code); }else{ echo '<p>There are no upcoming courses in this category.</p>'; } ?>
					</div>
				</div>
			</div>
		</div>

<?php } } ?>

<?php get_footer(); ?>