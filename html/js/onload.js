$(function() {
     // Menu
    $('.btn_menu').click(function() {
        $('.menu').addClass('show');
        $('.btn_close').addClass('show');
        $('.btn_menu').addClass('hide');
    });
    $('.btn_close ').click(function() {
        $('.menu').removeClass('show');
        $('.btn_close').removeClass('show');
        $('.btn_menu').removeClass('hide');
    });

    // uniform
     $(":radio, :checkbox").uniform();
    // Style all <select> elements
    // $("select").uniform();
    // Style everything
    // $("select, input, a.button, button").uniform();

});