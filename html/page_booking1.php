<!DOCTYPE html>
<html lang="en">
	<head>
		
		<meta charset="utf-8">
		<title>Site Name</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="SKYPE_TOOLBAR" CONTENT="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
		<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if lt IE 7]><script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script><![endif]-->
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="css/main.css">
		<!-- (for production, also delete previous line) <link rel="stylesheet" href="dist/css/main.css"> -->
	</head>
	<body class="page_pink footer_pink">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/page_maintaining_professional_boundaries_header.php"; ?>
		<!-- end header -->
		<div class="section content">
			<div class="row row_wrap">
				<div class="has_2_cols content">
					<div class="col col_left">
						<h2>Content Heading</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Donec pulvinar ligula ac pulvinar condimentum. Nulla id
							nisi aliquet,suscipit turpis vel, dignissim lacus. Aliquam
						vulputate, erat at.</p>
						<p>Ut lacinia enim est, non finibus tellus </p>
						<ul>
							<li>Lorem ipsum dolor</li>
							<li>Lorem ipsum dolor</li>
							<li>Lorem ipsum dolor</li>
							<li>Lorem ipsum dolor</li>
							<li>Lorem ipsum dolor</li>
							<li>Lorem ipsum dolor</li>
						</ul>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
							Donec pulvinar ligula ac pulvinar condimentum. Nulla id
							nisi aliquet,suscipit turpis vel, dignissim lacus. Aliquam
						vulputate, erat at.</p>
						<h2>Upcoming Course Dates</h2>
						<span class="course_dates date">Starting 1 December 2017</span>
						<span class="course_dates venue">London Chamber Of Commerce</span>
						<span class="course_dates date">Starting 1 December 2017</span>
						<span class="course_dates venue">London Chamber Of Commerce</span>
						<span class="course_dates date">Starting 1 December 2017</span>
						<span class="course_dates venue">London Chamber Of Commerce</span>
						<div class="pull_quote left">
							<?php include "templates/quote.php"; ?>
						</div>
					</div>
					<div class="col col_right has_form">
						<h2>Book Your Place Or Find Out More</h2>
						<form action="">
							<fieldset>
								<p>Please confirm that one or more of the following apply to you</p>
								<ul>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
								</ul>
								<div class="frm_opt_container">
									<div class="frm_radio">
										<label for="field_noslb-0">
											<input type="radio" name="item_meta[101]" id="field_noslb-0" value="Option 1"> Option 1
										</label>
									</div>
									
									<div class="frm_radio">
										<label for="field_noslb-1">
											<input type="radio" name="item_meta[101]" id="field_noslb-1" value="Option 2"> Option 2
										</label>
									</div>
								</div>
								<p>Do any of the following apply to you?</p>
								<ul>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
									<li>Lorem ipsum dolor</li>
								</ul>
								<div class="frm_opt_container">
									<div class="frm_radio">
										<label for="field_noslb-0">
											<input type="radio" name="item_meta[101]" id="field_noslb-0" value="Option 1"> Option 1
										</label>
									</div>
									
									<div class="frm_radio">
										<label for="field_noslb-1">
											<input type="radio" name="item_meta[101]" id="field_noslb-1" value="Option 2"> Option 2
										</label>
									</div>
								</div>
								<div class="submit">
									<a href="">[Book Now]</a>
								</div>
								<div class="response">
									<a href="">[It doesnt look like this course is right for you, but have a look a this one]</a>
									<a href="">[Find Out More]</a>
								</div>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
		
		<script src="dist/js/scripts.js"></script>
	</body>
</html>