jQuery(function($) {
     // Menu
    $('.btn_menu').click(function() {
        $('.menu').addClass('show');
        $('.btn_close').addClass('show');
        $('.btn_menu').addClass('hide');
    });
    $('.btn_close ').click(function() {
        $('.menu').removeClass('show');
        $('.btn_close').removeClass('show');
        $('.btn_menu').removeClass('hide');
    });

    // uniform
     $(":radio, :checkbox").uniform();
    // Style all <select> elements
    // $("select").uniform();
    // Style everything
    // $("select, input, a.button, button").uniform();

    $('body').on('click', '.nf-next', function(){
        
        setTimeout(function(){ $("select").uniform(); }, 1000);
    });


    $( document ).ready( function() {


        $( document )
        .on('click', ':input', function() {
            $('.nf-mp-footer').show();
        });
    
        // $( document )
        // .on('click', '.nf-previous', function() {
        //     $('.nf-form-cont').removeClass('hide_next');
        //     $('.nf-form-cont').addClass('hide_prev');
        // });
    
        // $( document )
        // .on('click', '.nf-next', function() {
        //     $('.nf-form-cont').removeClass('hide_prev');
        //     $('.nf-form-cont').addClass('hide_next');
        // });
    
        $( document )
        .on('click', '.nf-next', function() {
            $('.nf-form-cont').addClass('hide_footer');
        });
    
    
        $(".testimonials_slider").owlCarousel({
          loop: true,
          autoplay: true,
          items: 1,
          nav: true,
          autoplayHoverPause: true,
          autoplayTimeout: 10000
        });
    
    });
    

    if($('.vid_container').length){
        var BV = new $.BigVideo({
            useFlashForFirefox:false,
            controls:false,
            doLoop:true,
            forceAutoplay:true,
            container:$('.vid_container'),
        });
        BV.init();
        if (Modernizr.touch) {
            BV.show('images/background.jpg');
        } else {
            BV.show([
                { type: "video/mp4",  src: "https://professionalboundaries.org.uk/wp-content/themes/professional_boundaries/images/pb.mp4" },
                {ambient:true},
                ]);
        }
    }
    
});