<?php 
/*
	Template Name: Sidebar page
*/
get_header(); ?>


<?php if (have_posts()) { while (have_posts()) { the_post();  
	
$page_color = (get_field('page_color')) ? get_field('page_color') : 'page_pink';
	$footer_color = (get_field('footer_color')) ? get_field('footer_color') : 'footer_gold';
	?>

	<body class="<?php echo $page_color; ?> <?php echo $footer_color; ?>">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/about_header.php"; ?>
		<!-- end header -->

		<div class="section content">
			<div class="row">
				<div class="has_2_cols">
					<div class="col col-2_3">
						<?php the_content(); ?>

						<div class="pull_quote left pull_quote--sidebar">
							<?php 
							$slug = get_post_field( 'post_name', get_post() );
							$taxquery = array(
								array(
									'taxonomy' => 'testimonials_type',
									'field'    => 'slug',
									'terms'    => $slug,
								),
							);
							$args = array( 
								'post_type' => 'testimonial',
								'posts_per_page' => 1,
								'orderby' => 'rand',
								'tax_query' => $taxquery
							);
							$the_query = new WP_Query( $args );
							// The Loop
							if ( $the_query->have_posts() ) :
							while ( $the_query->have_posts() ) : $the_query->the_post();
							?>
<h2>What Our Clients Say...</h2>
							<div class="text"><?php the_content(); ?>
							</div>
							<div class="quote_by">
								<?php the_title(); ?>
							</div>

							<?php
							endwhile;
							endif;
							// Reset Post Data
							wp_reset_postdata();
							?>
						</div>
					</div>
					<div class="col col-1_3">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</div>
		
<?php } } ?>

<?php get_footer(); ?>