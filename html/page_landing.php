<!DOCTYPE html>
<html lang="en">
	<head>
		
		<meta charset="utf-8">
		<title>Site Name</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="SKYPE_TOOLBAR" CONTENT="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
		<!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
		<!--[if lt IE 7]><script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE7.js"></script><![endif]-->
		<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="stylesheet" href="css/main.css">
		<!-- (for production, also delete previous line) <link rel="stylesheet" href="dist/css/main.css"> -->
	</head>
	<body class="page_gold footer_gold">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/about_header.php"; ?>
		<!-- end header -->

		<div class="section content">
			<div class="row">
				<h2>Content Heading</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar ligula ac pulvinar condimentum. Nulla id nisi aliquet, 
				suscipit turpis vel, dignissim lacus. Aliquam vulputate, erat at egestas ullamcorper, quam massa dignissim leo, nec porta nulla 
				libero eu justo. Nulla ut sapien dignissim, fermentum arcu eget, luctus dolor. Nam leo arcu, facilisis vitae eleifend non, mattis 
				in magna. Maecenas fringilla nisl at laoreet ornare. In hac habitasse platea dictumst. Ut lacinia enim est, non finibus tellus </p>
				<ul>
					<li>Lorem ipsum dolor</li>
					<li>Lorem ipsum dolor</li>
					<li>Lorem ipsum dolor</li>
					<li>Lorem ipsum dolor</li>
					<li>Lorem ipsum dolor</li>
				</ul>

				<ul>
					<li>Lorem ipsum dolor</li>
					<li>Lorem ipsum dolor</li>
					<li>Lorem ipsum dolor</li>
					<li>Lorem ipsum dolor</li>
					<li>Lorem ipsum dolor</li>
				</ul>

				<div class="pull_quote full">
					<?php include "templates/quote.php"; ?>
				</div>

				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec pulvinar ligula ac pulvinar condimentum. Nulla id nisi aliquet, suscipit turpis vel, dignissim lacus.</p>

				<p>Aliquam vulputate, erat at egestas ullamcorper, quam massa dignissim leo, nec porta nulla libero eu justo. Nulla ut sapien dignissim, fermentum arcu eget, luctus dolor. Nam leo arcu, facilisis vitae eleifend non, mattis in magna. Maecenas fringilla nisl at laoreet ornare. In hac habitasse platea dictumst. Ut lacinia enim est, non finibus tellus
				</p>

				<div class="link_box">
					<a href="#">Apply For This Course Or General CTA Link</a>
				</div>
				
			</div>
		</div>

		<div class="section">
			<div class="row row_wrap">
				<h2 class="bordered">Upcoming Courses</h2>
				<div class="has_3_cols">
					<div class="col">
						<h4 class="title">Maintaining Professional Boundaries</h4>
						<div class="text">12-14 November <br>
						London Chamber Of Commerce</div>
						<div class="cta"><a href="page_maintaining_professional_boundaries.php">Book Now</a></div>
					</div>
					<div class="col">
						<h4 class="title">Maintaining Professional Boundaries</h4>
						<div class="text">12-14 November <br>
						London Chamber Of Commerce</div>
						<div class="cta"><a href="page_maintaining_professional_boundaries.php">Book Now</a></div>
					</div>
					<div class="col">
						<h4 class="title">Maintaining Professional Boundaries</h4>
						<div class="text">12-14 November <br>
						London Chamber Of Commerce</div>
						<div class="cta"><a href="page_maintaining_professional_boundaries.php">Book Now</a></div>
					</div>
				</div>
			</div>
		</div>
		
		<script src="dist/js/scripts.js"></script>
	</body>
</html>