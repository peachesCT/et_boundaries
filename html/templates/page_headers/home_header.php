<div class="section site_header">
	<div class="row row_wrap">
		<h1>We provide training and other services about professional conduct,<br>
		professional ethics and professional boundaries. <br>
		We help you to be safe at work.</h1>
		<div class="button-wrapper">
			<a class="big_button pink_bg" href="#">Discover Which Course Is Right For You</a>
		</div>
	</div>
</div>