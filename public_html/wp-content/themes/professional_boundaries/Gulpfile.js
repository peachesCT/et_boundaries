'use strict';
var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('sass', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({        
       outputStyle: 'compressed'
    }))
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('sassDev', function () {
  return gulp.src('./scss/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./css'));
});

gulp.task('sassDev:watch', function () {
  gulp.watch('./scss/**/*.scss', ['sassDev']);
});

gulp.task('scripts', function() {
  return gulp.src([
      //'./js/jquery-3.2.1.min.js',
      './js/jquery.uniform.min.js',
      './js/owl.carousel.min.js',
      './js/jquery-ui/jquery-ui.min.js',
      './js/imagesloaded/imagesloaded.pkgd.min.js',
      './js/video.js/dist/video.js',
      './js/BigVideo/lib/bigvideo.js',
      './js/onload.js'
    ])
    .pipe(plumber())
    .pipe(concat('scripts.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js/'));
});

gulp.task('scripts:watch', function () {
  gulp.watch('./js/*.js', ['scripts']);
});

gulp.task('default', ['sassDev', 'sassDev:watch', 'scripts', 'scripts:watch']);
gulp.task('production', ['sass']);