<?php include "templates/footer.php"; ?>

<!-- Modal HTML embedded directly into document -->
<style>
	.nf-field-element input[type=tel] {
	    border: 1px solid #212121;
	    padding: 9px 11px;
	    -webkit-appearance: none;
	    border-radius: 0;
	    color: #333;
	    font-size: 14px;
	    font-weight: 300;
	    outline: 0;
	}
	.list-select-wrap .nf-field-element{
		background: url(<?php bloginfo('template_url'); ?>/images/down-arrow.png) no-repeat right 10px center / 10px;
	}
	.list-select-wrap .nf-field-element select{
		background:none;
	}
</style>


<?php wp_footer(); ?>

<script src="<?php bloginfo('stylesheet_directory'); ?>/dist/js/scripts.js?ver=<?php echo time(); ?>"></script>

<!-- jQuery Modal -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>

<!-- begin Moneypenny code -->
        
<script type="text/javascript">
    (function() {
    var se = document.createElement('script');
    se.type = 'text/javascript';
    se.async = true;
    se.src = "https://storage.googleapis.com/moneypennychat/js/9db61a0c-dc5f-47bc-befb-e3bdd9986fd3.js";
    var done = false;
    se.onload = se.onreadystatechange = function() {
        if (!done && (!this.readyState || this.readyState === 'loaded' || this.readyState === 'complete')) {
        done = true;
        Moneypenny.setCallback('StartChat', function (email, msg, type) {
            ga('send', 'event', 'Moneypenny', 'startchat', type);
        });
        Moneypenny.setCallback('OpenProactive', function(agent, msg) {
            ga('send', 'event', 'Moneypenny', 'proactiveopened', agent);
        });
        Moneypenny.setCallback('MessageSubmit', function(email, msg) {
            ga('send', 'event', 'Moneypenny', 'offline');
        });
        Moneypenny.setCallback('ChatMessageReceived', function(agent, msg) {
            ga('send', 'event', 'Moneypenny', 'agentmessage', agent);
        });
        Moneypenny.setCallback('ChatMessageSent', function(msg) {
            ga('send', 'event', 'Moneypenny', 'visitormessage');
        });
        Moneypenny.setCallback('Close', function(agent, status) {
            ga('send', 'event', 'Moneypenny', 'closechat');
        });
        Moneypenny.setCallback('Open', function(status) {
            ga('send', 'event', 'Moneypenny', 'openworks');
        });
        Moneypenny.setCallback('Minimize', function (isMinimized, chatType, boxType) {
            ga('send', 'event', 'Moneypenny', 'minimizechat');
        });
    }
    };
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(se, s);
    })();
</script>
        
<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o), m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-18347604-1', 'auto');
</script>
        
<!-- end Moneypenny code -->
        
                    

<!-- This code does custom validation on the forms on following pages, have to apply radio button ids manually for them to work. Without following code there is a bug in the plugin where next button displays before both radios are selected -->
<?php if(is_page('Maintaining Professional Boundaries Booking Form') || is_page('Maintaining Professional Ethics Booking Form')){ ?>
<script>

	<?php 
		if(is_page('Maintaining Professional Boundaries Booking Form')){
			echo 'var radio1 = "29";';
			echo 'var radio2 = "30";';
		}
		if(is_page('Maintaining Professional Ethics Booking Form')){
			echo 'var radio1 = "43";';
			echo 'var radio2 = "42";';
		}
	?>

	if(typeof(Marionette) !== 'undefined'){

		localStorage.removeItem("radio1");
		localStorage.removeItem("radio2");


		var myCustomFieldController = Marionette.Object.extend({
	  
		    initialize: function() {

		        // On the Form Submission's field validaiton...
		        var submitChannel = Backbone.Radio.channel( 'submit' );
		        this.listenTo( submitChannel, 'validate:field', this.validate );

		        // on the Field's model value change...
		        var fieldsChannel = Backbone.Radio.channel( 'fields' );
		        this.listenTo( fieldsChannel, 'change:modelValue', this.validate );

		    },
		    
		    validate: function( model ) {

		    	//console.log(model.get( 'value' ));
		    	//console.log(model.get( 'id' ));

		    	jQuery('body').on('change','input', function(){
		    		var id = $(this).attr('name');
		    		var val = $(this).val();
		    		// console.log(id);
		    		// console.log(val);

		    		if(id == 'nf-field-'+radio1){
						localStorage.setItem("radio1", val);
		    		}else if(id == 'nf-field-'+radio2){
						localStorage.setItem("radio2", val);
		    		}

		    		console.log(localStorage);

		    		if(typeof(localStorage.radio1) !== 'undefined' &&  typeof(localStorage.radio2) !== 'undefined'){
		    			$('.nf-mp-footer').show();
		    		}else{
		    			$('.nf-mp-footer').hide();
		    		}
		    		
		    	});

		    }
		  
		});

		jQuery( document ).ready( function( $ ) {
		    new myCustomFieldController();
		});
	}
</script>
<?php } ?>

<!-- <?php //if(is_page('our-courses') || is_front_page()){ ?>
	<script>
		const seenmodal = localStorage.getItem("seenmodal");
		if( !seenmodal || (Math.floor(Date.now() / 1000) - seenmodal) > 3600 ){
			setTimeout(function(){ 
				jQuery('#manual-ajax').click();
				localStorage.setItem("seenmodal", Math.floor(Date.now() / 1000));
			}, 10000);
		}
	</script>
<?php //} ?> -->

</body>
</html>