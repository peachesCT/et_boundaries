<?php
	
	/**  
	 * Project Functions
	 */
	
	// add woocommerce support
	/*add_theme_support( 'woocommerce' );*/

	// //preprocess form data
	// add_filter( 'ninja_forms_submit_data', 'my_ninja_forms_submit_data' );
	// function my_ninja_forms_submit_data( $form_data ) {
	 
	//   foreach( $form_data[ 'fields' ] as $field ) { // Field settigns, including the field key and value.
	   
	// 	  if($field[ 'value' ] == 'Organisation'){
	// 	  	$field[ 'value' ] = 'NONE';
	// 	  	mail('etienne.home@gmail.com', 'dump', print_r($field, true));
	// 	  }

	//   }
	  
	//   return $form_data;
	// }


	//autofiill prefferred course date
	add_filter( 'ninja_forms_render_options', function($options,$settings){
	   if( $settings['key'] == 'preferred_course_date_mpb' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'maintaining-professional-boundaries',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Maintaining Professional Boundaries - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter
	   if( $settings['key'] == 'preferred_course_date_mpe' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'maintaining-professional-ethics',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Maintaining Professional Ethics - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter
	   if( $settings['key'] == 'preferred_course_date_bip' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'professional-boundaries-in-practice',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Professional Boundaries in Practice - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter
	    if( $settings['key'] == 'preferred_course_date_lip' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'listening-in-practice',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Listening In Practice - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter
	   if( $settings['key'] == 'preferred_course_date_mpr' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'maintaining-professional-relationships',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Maintaining Professional Relationships - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter
	   if( $settings['key'] == 'preferred_course_date_vip' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'values-in-practice',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Values In Practice - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter
	   if( $settings['key'] == 'preferred_course_date_mp4th' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'maintaining-professionalism-the-fourth-day',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Maintaining Professionalism the 4th Day - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter
	   if( $settings['key'] == 'preferred_course_date_itpb' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'introduction-to-professional-boundaries',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Introduction to Professional Boundaries - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter
	   if( $settings['key'] == 'preferred_course_date_mpr' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'maintaining-professional-relationships',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Maintaining Professional Relationships - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter
	   if( $settings['key'] == 'preferred_course_date_mpres' ){
	   		$taxquery = array(
				array(
					'taxonomy' => 'course_type',
					'field'    => 'slug',
					'terms'    => 'maintaining-professional-resilience',
				),
			);
	       $args = array(
	           'post_type' => 'course',
			   'posts_per_page' => -1,
			   'tax_query' => $taxquery,
	           'post_status' => 'publish',
	           'meta_key' => 'start_date', 
	           'orderby' => 'meta_value', 
	           'order' => 'ASC'
	       );
	       $the_query = new WP_Query( $args ); 
	       if ( $the_query->have_posts() ){
	           global $post;
	           while ( $the_query->have_posts() ){
	               $the_query->the_post();
	               $start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
	               $title = 'Maintaining Professional Resilience - '.$start_date;
	               $options[] = array('label' => $title, 'value' => $title);
	           }
	           wp_reset_postdata(); 
	       }
	   }//end filter

	   
	   return $options;
	},10,2);



	function wpb_widgets_init() {
	 
	    register_sidebar( array(
	        'name' => __( 'Main Sidebar', 'wpb' ),
	        'id' => 'sidebar-1',
	        'description' => __( 'The main sidebar appears on the right on each page except the front page template', 'wpb' ),
	        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	        'after_widget' => '</aside>',
	        'before_title' => '<h3 class="widget-title">',
	        'after_title' => '</h3>',
	    ) );
	 

	    }
	 
	add_action( 'widgets_init', 'wpb_widgets_init' );



	/**  
	 * Formidable Functions
	 */

	// make disclaimer all required
	// add_filter('frm_validate_field_entry', 'my_custom_validation', 10, 3);
	// function my_custom_validation($errors, $posted_field, $posted_value){
	// 	if($posted_field->id == 105) {
	// 		if(!is_array($posted_value) or count($posted_value) < 5) {
	// 			$errors['field'. $posted_field->id] = 'That field is wrong!';
	// 		}
	// 	}
	// 	return $errors;
	// }



	// password validation
	// add_filter('frm_validate_field_entry', 'password_validation', 20, 3);
	// function password_validation($errors, $field, $value){
	// 	if ($field->id == 109){
	// 		$first_value = $_POST['item_meta'][90];

	// 		if ( $first_value != $value && !empty($value) ) {
	// 			$errors['field'. $field->id] = 'The passwords entered do not match.';
	// 		} else {
	// 			$_POST['item_meta'][$field->id] = '';
	// 		}
	// 	}
	// 	return $errors;
	// }



	/**  
	 * WordPress Functions
	 */


	// disable admin bar
	show_admin_bar(false);
	

	// enable custom menu support
	if (function_exists('add_theme_support')) {
		add_theme_support( 'menus' );
	}



	// wp_admin_css_color(
	// 	'ninja',
	// 	__('Ninja'),
	// 	admin_url("css/colors-fresh.css"),
	// 	array('#222222', '#333333', '#f26724', '#f6996c')
	// );
	// set as default color scheme
	// add_filter( 'get_user_option_admin_color', 'update_user_option_admin_color', 5 );
	// function update_user_option_admin_color( $color_scheme ) {
	// 	$color_scheme = 'ninja';
	// 	return $color_scheme;
	// }


	// enable custom background
	/*add_custom_background();*/

	// add custom post type - course
	function courseRegister(){
		
		$singular = 'Course';
		$plural = 'Courses';
		$posttype = 'course';

		$labels = array(
			'name' => _x($plural, 'post type general name'),
			'singular_name' => _x($singular, 'post type singular name'),
			'add_new' => _x('Add New', strtolower($singular)),
			'add_new_item' => __('Add New '.$singular),
			'edit_item' => __('Edit '.$singular),
			'new_item' => __('New '.$singular),
			'view_item' => __('View '.$singular),
			'search_items' => __('Search '.$plural),
			'not_found' => __('Nothing found'),
			'not_found_in_trash' => __('Nothing found in Trash'),
			'parent_item_colon' => ''
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array('title', 'editor', 'thumbnail'),
			'rewrite' => true,
			'show_in_nav_menus' => true,
			'has_archive' => true
		);
		
		register_post_type($posttype , $args);
		
	}
	add_action('init', 'courseRegister');


	// add custom post type - testimonial
	function testimonialRegister(){
		
		$singular = 'Testimonial';
		$plural = 'Testimonials';
		$posttype = 'testimonial';

		$labels = array(
			'name' => _x($plural, 'post type general name'),
			'singular_name' => _x($singular, 'post type singular name'),
			'add_new' => _x('Add New', strtolower($singular)),
			'add_new_item' => __('Add New '.$singular),
			'edit_item' => __('Edit '.$singular),
			'new_item' => __('New '.$singular),
			'view_item' => __('View '.$singular),
			'search_items' => __('Search '.$plural),
			'not_found' => __('Nothing found'),
			'not_found_in_trash' => __('Nothing found in Trash'),
			'parent_item_colon' => ''
		);
		$args = array(
			'labels' => $labels,
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array('title', 'editor', 'thumbnail'),
			'rewrite' => true,
			'show_in_nav_menus' => true,
			'has_archive' => true
		);
		
		register_post_type($posttype , $args);
		
	}
	add_action('init', 'testimonialRegister');
	

	// add custom taxonomy - course_type
	function course_type_taxonomies() {
		
		$singular = 'Course Type';
		$plural = 'Course Types';
		$taxterm = 'course_type';

		register_taxonomy($taxterm, array('course', 'testimonial'), array(
			'hierarchical' => true,
			'labels' => array(
				'name' => _x( $plural, 'taxonomy general name' ),
				'singular_name' => _x( $singular, 'taxonomy singular name' ),
				'search_items' =>  __( 'Search '.$plural ),
				'all_items' => __( 'All '.$plural ),
				'parent_item' => __( 'Parent '.$singular ),
				'parent_item_colon' => __( 'Parent '.$singular.':' ),
				'edit_item' => __( 'Edit '.$singular ),
				'update_item' => __( 'Update '.$singular ),
				'add_new_item' => __( 'Add New '.$singular ),
				'new_item_name' => __( 'New '.$singular.' Name' ),
				'menu_name' => __( $plural ),
			),
			'rewrite' => array(
				'slug' => $taxterm,
				'with_front' => false,
				'hierarchical' => true
			),
		));
	}
	add_action( 'init', 'course_type_taxonomies', 0 );


		// add custom taxonomy - testimonials
	function testimonials_taxonomies() {
		
		$singular = 'Testimonial Type';
		$plural = 'Testimonial Types';
		$taxterm = 'testimonials_type';

		register_taxonomy($taxterm, array('testimonial'), array(
			'hierarchical' => true,
			'labels' => array(
				'name' => _x( $plural, 'taxonomy general name' ),
				'singular_name' => _x( $singular, 'taxonomy singular name' ),
				'search_items' =>  __( 'Search '.$plural ),
				'all_items' => __( 'All '.$plural ),
				'parent_item' => __( 'Parent '.$singular ),
				'parent_item_colon' => __( 'Parent '.$singular.':' ),
				'edit_item' => __( 'Edit '.$singular ),
				'update_item' => __( 'Update '.$singular ),
				'add_new_item' => __( 'Add New '.$singular ),
				'new_item_name' => __( 'New '.$singular.' Name' ),
				'menu_name' => __( $plural ),
			),
			'rewrite' => array(
				'slug' => $taxterm,
				'with_front' => false,
				'hierarchical' => true
			),
		));
	}
	add_action( 'init', 'testimonials_taxonomies', 0 );
	

	// enable post thumbnail theme support
	/*if (function_exists('add_theme_support')) {
		add_theme_support('post-thumbnails');
		set_post_thumbnail_size(150, 150, true);
		add_image_size('news-thumbnail', 75, 75, true);
	}*/
	
	
	// register sidebar widgets
	/*if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'name'=>'MiddleSidebar',
			'before_widget' => '<li class="widget">',
			'after_widget' => '</li>',
			'before_title' => '<h2 class="widgettitle">',
			'after_title' => '</h3>'
		));
		register_sidebar(array(
			'name'=>'FooterSidebar',
			'before_widget' => '<li class="widget">',
			'after_widget' => '</li>',
			'before_title' => '<h2 class="widgettitle">',
			'after_title' => '</h3>'
		));
	}*/




		//Display a custom taxonomy dropdown in admin
		function tsm_filter_post_type_by_taxonomy_posts() {
			global $typenow;
			$post_type = 'testimonial'; // change to your post type
			$taxonomy  = 'course_type'; // change to your taxonomy
			if ($typenow == $post_type) {
				$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
				$info_taxonomy = get_taxonomy($taxonomy);
				wp_dropdown_categories(array(
					'show_option_all' => __("Show All {$info_taxonomy->label}"),
					'taxonomy'        => $taxonomy,
					'name'            => $taxonomy,
					'orderby'         => 'name',
					'selected'        => $selected,
					'show_count'      => true,
					'hide_empty'      => true,
					'hierarchical'       => true,
					'depth'              => 0,
				));
			};
		}//tsm_filter_post_type_by_taxonomy_posts
		add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy_posts');


		//Filter posts by taxonomy in admin
		function tsm_convert_id_to_term_in_query_posts($query) {
			global $pagenow;
			$post_type = 'testimonial'; // change to your post type
			$taxonomy  = 'course_type'; // change to your taxonomy
			$q_vars    = &$query->query_vars;
			if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
				$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
				$q_vars[$taxonomy] = $term->slug;
			}
		}//tsm_convert_id_to_term_in_query_posts
		add_filter('parse_query', 'tsm_convert_id_to_term_in_query_posts');


		//add taxonomy column to magazines
		function testimonial_columns( $taxonomies ) {
		    $taxonomies[] = 'course_type';
		    return $taxonomies;
		}
		add_filter( 'manage_taxonomies_for_testimonial_columns', 'testimonial_columns');
	
	
?>