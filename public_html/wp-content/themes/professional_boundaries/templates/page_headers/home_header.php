<div class="section site_header">
	<div class="row row_wrap">
		<h1>Training and other services for professionals facing difficulties relating to professional boundaries, ethics, conduct and career challenges. We help you to stay safe at work.</h1>
		<div class="button-wrapper">
			<a class="big_button pink_bg" href="<?php bloginfo('url'); ?>/our-courses/">Discover Which Course Is Right For You</a>
		</div>
	</div>
</div>