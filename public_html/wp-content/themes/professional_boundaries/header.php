<!DOCTYPE html> 
<html lang="en">
<head>
	
	<meta charset="utf-8">
	
	<title><?php bloginfo('name'); ?> <?php wp_title('|', true, 'left'); ?></title> 
	
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<meta name="SKYPE_TOOLBAR" CONTENT="SKYPE_TOOLBAR_PARSER_COMPATIBLE">
	
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
	<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js "></script>

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />

	<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/css/main.css?ver=<?php echo time(); ?>">
	
	<?php wp_head(); ?>

</head>