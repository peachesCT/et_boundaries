<?php 
/*Template name: Contact page*/
get_header(); ?>

<?php if (have_posts()) { while (have_posts()) { the_post(); 
	$form_code = (get_field('form_code')) ? get_field('form_code') : '' ;
	$page_color = (get_field('page_color')) ? get_field('page_color') : 'page_pink';
	$footer_color = (get_field('footer_color')) ? get_field('footer_color') : 'footer_gold';
?>

	<body class="<?php echo $page_color; ?> <?php echo $footer_color; ?>">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<div class="section site_header">
			<div class="row">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
		<!-- end header -->
		<div class="section content">
			<div class="row row_wrap">
				<div class="has_2_cols content">
					<div class="col col_left">
						<?php the_content(); ?>

						<div class="pull_quote left">
							<?php 
							$args = array( 
								'post_type' => 'testimonial',
								'posts_per_page' => 1,
								'orderby' => 'rand'
							);
							$the_query = new WP_Query( $args );
							// The Loop
							if ( $the_query->have_posts() ) :
							while ( $the_query->have_posts() ) : $the_query->the_post();
							?>
<h2>What Our Clients Say...</h2>
							<div class="text"><?php the_content(); ?>
							</div>
							<div class="quote_by">
								<?php the_title(); ?>
							</div>

							<?php
							endwhile;
							endif;
							// Reset Post Data
							wp_reset_postdata();
							?>
						</div>
					</div>
					<div class="col col_right has_form">
						<h2>Send us a message</h2>
						<?php if($form_code != '' && !$nocourses ){ echo do_shortcode($form_code); } ?>
					</div>
				</div>
			</div>
		</div>

<?php } } ?>

<?php get_footer(); ?>