<h2>What Our Clients Say...</h2>


<?php 
$testimonial_slider_course_category = get_field('testimonial_slider_course_category');
$testimonial_slider_category = get_field('testimonial_slider_category');


if(is_front_page()){
	$args = array( 
		'post_type' => 'testimonial',
		'posts_per_page' => -1,
		'meta_query' => array (
		    array (
		        //'relation' => 'OR',
		        'key' => 'is_home', //The field to check.
		        'value' => true, //The value of the field.
		        'compare' => 'LIKE', //Conditional statement used on the value.
		    ),  
		),
	);
}elseif($testimonial_slider_course_category){

	$taxquery = array(
		array(
			'taxonomy' => 'course_type',
			'field'    => 'term_id',
			'terms'    => $testimonial_slider_course_category,
		),
	);
	$args = array( 
		'post_type' => 'testimonial',
		'posts_per_page' => -1,
		'orderby' => 'rand',
		'tax_query' => $taxquery
	);
}elseif($testimonial_slider_category){

	$taxquery = array(
		array(
			'taxonomy' => 'testimonials_type',
			'field'    => 'term_id',
			'terms'    => $testimonial_slider_category,
		),
	);
	$args = array( 
		'post_type' => 'testimonial',
		'posts_per_page' => -1,
		'orderby' => 'rand',
		'tax_query' => $taxquery
	);
}else{
	$args = array( 
		'post_type' => 'testimonial',
		'posts_per_page' => -1,
		'orderby' => 'rand'
	);
}

$the_query = new WP_Query( $args );
// The Loop
if ( $the_query->have_posts() ) :
	echo '<div class="testimonials_slider owl-carousel">';
while ( $the_query->have_posts() ) : $the_query->the_post();
	$display_title = (get_field('display_title')) ? get_field('display_title') : '';
?>
<div class="item">
	<div class="text"><?php the_content(); ?>
	</div>
	<div class="quote_by">
		<?php echo $display_title; ?>
	</div>
</div>


<?php
endwhile;
	echo '</div>';
endif;
// Reset Post Data
wp_reset_postdata();
?>