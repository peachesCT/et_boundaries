<?php 
/*
	Template Name: Course page
*/
get_header(); ?>

<?php if (have_posts()) { while (have_posts()) { the_post(); 

	$bottom_text = (get_field('bottom_text')) ? get_field('bottom_text') : '';
	$page_color = (get_field('page_color')) ? get_field('page_color') : 'page_pink';
	$footer_color = (get_field('footer_color')) ? get_field('footer_color') : 'footer_gold';
?>

	<body class="<?php echo $page_color; ?> <?php echo $footer_color; ?>">
		<!-- start header -->
		<?php include "templates/nav.php"; ?>
		<?php include "templates/page_headers/about_header.php"; ?>
		<!-- end header -->

							<?php 
						$pagelist = get_pages("child_of=".$post->ID."&parent=".$post->ID."&sort_column=menu_order&sort_order=asc");
						$formpagelink = get_permalink($pagelist[0]->ID);
						$link_course_type = get_field('link_course_type');
						$pagelink = get_the_permalink();
						$taxquery = array(
							array(
								'taxonomy' => 'course_type',
								'field'    => 'slug',
								'terms'    => $link_course_type->slug,
							),
						);

						$args = array(
							'post_type' => 'course',
							'posts_per_page' => 3,
							'tax_query' => $taxquery,
							'meta_key' => 'start_date', 
							'orderby' => 'meta_value', 
							'order' => 'ASC'
						); 

						$the_query = new WP_Query( $args );
						// The Loop
						if ( $the_query->have_posts() ) : ?>


		<div class="section" style="padding-top:50px;">
			<div class="row row_wrap">
				<h2 class="bordered">Upcoming Courses</h2>
				<div class="has_3_cols">

							<?php
						while ( $the_query->have_posts() ) : $the_query->the_post();

						$start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
						$date_text = (get_field('date_text')) ? get_field('date_text') : '' ;
						$address = (get_field('address')) ? get_field('address') : '' ;
					?>

					<div class="col">
						<h4 class="title"><?php the_title(); ?></h4>
						<div class="text">
						<?php echo ($date_text) ? $date_text . '<br>' : ''; ?>
						<?php echo $address; ?></div>
						<div class="cta"><a href="<?php echo $formpagelink; ?>">Enquire Now</a></div>
					</div>

					<?php endwhile; ?>


				</div>
			</div>
		</div>

					<?php	endif;
						// Reset Post Data
						wp_reset_postdata();
					?>

			<?php if(get_field('see_more_link')){ ?>
				<div class="link_box" style="padding-top:0;"><a href="<?php echo get_field('see_more_link') ?>">Read Reviews</a></div>
				<?php } ?>

		<div class="section content">
			<div class="row">
				<?php the_content(); ?>

				<div class="pull_quote full">
					<?php include "templates/quote.php"; ?>
				</div>

				<?php if(get_field('see_more_link')){ ?>
				<div class="link_box" style="padding-top:0;"><a href="<?php echo get_field('see_more_link') ?>">Read More Reviews</a></div>
				<?php } ?>

				<?php echo $bottom_text; ?>

				<?php if(get_field('cta_link') && get_field('cta_link_text')){ ?>
				<div class="link_box">
					<a href="<?php echo get_field('cta_link'); ?>"><?php echo get_field('cta_link_text'); ?></a>
				</div>
				<?php } ?>
				
			</div>
		</div>


	<?php 
						$pagelist = get_pages("child_of=".$post->ID."&parent=".$post->ID."&sort_column=menu_order&sort_order=asc");
						$formpagelink = get_permalink($pagelist[0]->ID);
						$link_course_type = get_field('link_course_type');
						$pagelink = get_the_permalink();
						$taxquery = array(
							array(
								'taxonomy' => 'course_type',
								'field'    => 'slug',
								'terms'    => $link_course_type->slug,
							),
						);

						$args = array(
							'post_type' => 'course',
							'posts_per_page' => 3,
							'tax_query' => $taxquery,
							'meta_key' => 'start_date', 
							'orderby' => 'meta_value', 
							'order' => 'ASC'
						); 

						$the_query = new WP_Query( $args );
						// The Loop
						if ( $the_query->have_posts() ) : ?>


		<div class="section">
			<div class="row row_wrap">
				<h2 class="bordered">Upcoming Courses</h2>
				<div class="has_3_cols">

							<?php
						while ( $the_query->have_posts() ) : $the_query->the_post();

						$start_date = (get_field('start_date')) ? get_field('start_date') : '' ;
						$date_text = (get_field('date_text')) ? get_field('date_text') : '' ;
						$address = (get_field('address')) ? get_field('address') : '' ;
					?>

					<div class="col">
						<h4 class="title"><?php the_title(); ?></h4>
						<div class="text"><?php echo $date_text; ?><br>
						<?php echo $address; ?></div>
						<div class="cta"><a href="<?php echo $formpagelink; ?>">Enquire Now</a></div>
					</div>

					<?php endwhile; ?>


				</div>
			</div>
		</div>

					<?php	endif;
						// Reset Post Data
						wp_reset_postdata();
					?>



<?php } } ?>

<?php get_footer(); ?>