<?php
	
	/**  
	 * Ninja Framework
	 * 
	 * @version 1.009
	 * @author Ninjas for Hire <info@ninjasforhire.co.za>
	 * 
	 * Made in Cape Town :)
	 */


	// define wordpress globals
	global $wpdb;
	global $table_prefix;
	global $menu;
	global $submenu;
	global $bloginfo;


	// custom login logo
	function loginlogo() {
		echo '
			<style type="text/css">
				#login h1 a {background-image: url('.get_bloginfo('template_directory').'/_ninja-framework/images/ninja-logo.svg) !important; }
			</style>
		';
	}
	add_action('login_head', 'loginlogo');

	
	// include nf-class
	include "nf-image.php";
	include "nf-class.php";

 	$nf = new nf;